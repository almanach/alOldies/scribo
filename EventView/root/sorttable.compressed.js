var stIsIE=false;
sorttable={init:function(){
if(arguments.callee.done){
return;
}
arguments.callee.done=true;
if(_timer){
clearInterval(_timer);
}
if(!document.createElement||!document.getElementsByTagName){
return;
}
sorttable.DATE_RE=/^(\d\d?)[\/\.-](\d\d?)[\/\.-]((\d\d)?\d\d)$/;
forEach(document.getElementsByTagName("table"),function(_1){
if(_1.className.search(/\bsortable\b/)!=-1){
sorttable.makeSortable(_1);
}
});
},makeSortable:function(_2){
if(_2.getElementsByTagName("thead").length==0){
the=document.createElement("thead");
the.appendChild(_2.rows[0]);
_2.insertBefore(the,_2.firstChild);
}
if(_2.tHead==null){
_2.tHead=_2.getElementsByTagName("thead")[0];
}
if(_2.tHead.rows.length!=1){
return;
}
sortbottomrows=[];
for(var i=0;i<_2.rows.length;i++){
if(_2.rows[i].className.search(/\bsortbottom\b/)!=-1){
sortbottomrows[sortbottomrows.length]=_2.rows[i];
}
}
if(sortbottomrows){
if(_2.tFoot==null){
tfo=document.createElement("tfoot");
_2.appendChild(tfo);
}
for(var i=0;i<sortbottomrows.length;i++){
tfo.appendChild(sortbottomrows[i]);
}
delete sortbottomrows;
}
headrow=_2.tHead.rows[0].cells;
for(var i=0;i<headrow.length;i++){
if(!headrow[i].className.match(/\bsorttable_nosort\b/)){
mtch=headrow[i].className.match(/\bsorttable_([a-z0-9]+)\b/);
if(mtch){
override=mtch[1];
}
if(mtch&&typeof sorttable["sort_"+override]=="function"){
headrow[i].sorttable_sortfunction=sorttable["sort_"+override];
}else{
headrow[i].sorttable_sortfunction=sorttable.guessType(_2,i);
}
headrow[i].sorttable_columnindex=i;
headrow[i].sorttable_tbody=_2.tBodies[0];
dean_addEvent(headrow[i],"click",function(e){
if(this.className.search(/\bsorttable_sorted\b/)!=-1){
sorttable.reverse(this.sorttable_tbody);
this.className=this.className.replace("sorttable_sorted","sorttable_sorted_reverse");
this.removeChild(document.getElementById("sorttable_sortfwdind"));
sortrevind=document.createElement("span");
sortrevind.id="sorttable_sortrevind";
sortrevind.innerHTML=stIsIE?"&nbsp<font face=\"webdings\">5</font>":"&nbsp;&#x25B4;";
this.appendChild(sortrevind);
return;
}
if(this.className.search(/\bsorttable_sorted_reverse\b/)!=-1){
sorttable.reverse(this.sorttable_tbody);
this.className=this.className.replace("sorttable_sorted_reverse","sorttable_sorted");
this.removeChild(document.getElementById("sorttable_sortrevind"));
sortfwdind=document.createElement("span");
sortfwdind.id="sorttable_sortfwdind";
sortfwdind.innerHTML=stIsIE?"&nbsp<font face=\"webdings\">6</font>":"&nbsp;&#x25BE;";
this.appendChild(sortfwdind);
return;
}
theadrow=this.parentNode;
forEach(theadrow.childNodes,function(_7){
if(_7.nodeType==1){
_7.className=_7.className.replace("sorttable_sorted_reverse","");
_7.className=_7.className.replace("sorttable_sorted","");
}
});
sortfwdind=document.getElementById("sorttable_sortfwdind");
if(sortfwdind){
sortfwdind.parentNode.removeChild(sortfwdind);
}
sortrevind=document.getElementById("sorttable_sortrevind");
if(sortrevind){
sortrevind.parentNode.removeChild(sortrevind);
}
this.className+=" sorttable_sorted";
sortfwdind=document.createElement("span");
sortfwdind.id="sorttable_sortfwdind";
sortfwdind.innerHTML=stIsIE?"&nbsp<font face=\"webdings\">6</font>":"&nbsp;&#x25BE;";
this.appendChild(sortfwdind);
row_array=[];
col=this.sorttable_columnindex;
rows=this.sorttable_tbody.rows;
for(var j=0;j<rows.length;j++){
row_array[row_array.length]=[sorttable.getInnerText(rows[j].cells[col]),rows[j]];
}
row_array.sort(this.sorttable_sortfunction);
tb=this.sorttable_tbody;
for(var j=0;j<row_array.length;j++){
tb.appendChild(row_array[j][1]);
}
delete row_array;
});
}
}
},guessType:function(_a,_b){
sortfn=sorttable.sort_alpha;
for(var i=0;i<_a.tBodies[0].rows.length;i++){
text=sorttable.getInnerText(_a.tBodies[0].rows[i].cells[_b]);
if(text!=""){
if(text.match(/^-?[?$?]?[\d,.]+%?$/)){
return sorttable.sort_numeric;
}
possdate=text.match(sorttable.DATE_RE);
if(possdate){
first=parseInt(possdate[1]);
second=parseInt(possdate[2]);
if(first>12){
return sorttable.sort_ddmm;
}else{
if(second>12){
return sorttable.sort_mmdd;
}else{
sortfn=sorttable.sort_ddmm;
}
}
}
}
}
return sortfn;
},getInnerText:function(_d){
hasInputs=(typeof _d.getElementsByTagName=="function")&&_d.getElementsByTagName("input").length;
if(_d.getAttribute("sorttable_customkey")!=null){
return _d.getAttribute("sorttable_customkey");
}else{
if(typeof _d.textContent!="undefined"&&!hasInputs){
return _d.textContent.replace(/^\s+|\s+$/g,"");
}else{
if(typeof _d.innerText!="undefined"&&!hasInputs){
return _d.innerText.replace(/^\s+|\s+$/g,"");
}else{
if(typeof _d.text!="undefined"&&!hasInputs){
return _d.text.replace(/^\s+|\s+$/g,"");
}else{
switch(_d.nodeType){
case 3:
if(_d.nodeName.toLowerCase()=="input"){
return _d.value.replace(/^\s+|\s+$/g,"");
}
case 4:
return _d.nodeValue.replace(/^\s+|\s+$/g,"");
break;
case 1:
case 11:
var _e="";
for(var i=0;i<_d.childNodes.length;i++){
_e+=sorttable.getInnerText(_d.childNodes[i]);
}
return _e.replace(/^\s+|\s+$/g,"");
break;
default:
return "";
}
}
}
}
}
},reverse:function(_10){
newrows=[];
for(var i=0;i<_10.rows.length;i++){
newrows[newrows.length]=_10.rows[i];
}
for(var i=newrows.length-1;i>=0;i--){
_10.appendChild(newrows[i]);
}
delete newrows;
},sort_numeric:function(a,b){
aa=parseFloat(a[0].replace(/[^0-9.-]/g,""));
if(isNaN(aa)){
aa=0;
}
bb=parseFloat(b[0].replace(/[^0-9.-]/g,""));
if(isNaN(bb)){
bb=0;
}
return aa-bb;
},sort_alpha:function(a,b){
if(a[0]==b[0]){
return 0;
}
if(a[0]<b[0]){
return -1;
}
return 1;
},sort_ddmm:function(a,b){
mtch=a[0].match(sorttable.DATE_RE);
y=mtch[3];
m=mtch[2];
d=mtch[1];
if(m.length==1){
m="0"+m;
}
if(d.length==1){
d="0"+d;
}
dt1=y+m+d;
mtch=b[0].match(sorttable.DATE_RE);
y=mtch[3];
m=mtch[2];
d=mtch[1];
if(m.length==1){
m="0"+m;
}
if(d.length==1){
d="0"+d;
}
dt2=y+m+d;
if(dt1==dt2){
return 0;
}
if(dt1<dt2){
return -1;
}
return 1;
},sort_mmdd:function(a,b){
mtch=a[0].match(sorttable.DATE_RE);
y=mtch[3];
d=mtch[2];
m=mtch[1];
if(m.length==1){
m="0"+m;
}
if(d.length==1){
d="0"+d;
}
dt1=y+m+d;
mtch=b[0].match(sorttable.DATE_RE);
y=mtch[3];
d=mtch[2];
m=mtch[1];
if(m.length==1){
m="0"+m;
}
if(d.length==1){
d="0"+d;
}
dt2=y+m+d;
if(dt1==dt2){
return 0;
}
if(dt1<dt2){
return -1;
}
return 1;
},shaker_sort:function(_1b,_1c){
var b=0;
var t=_1b.length-1;
var _1f=true;
while(_1f){
_1f=false;
for(var i=b;i<t;++i){
if(_1c(_1b[i],_1b[i+1])>0){
var q=_1b[i];
_1b[i]=_1b[i+1];
_1b[i+1]=q;
_1f=true;
}
}
t--;
if(!_1f){
break;
}
for(var i=t;i>b;--i){
if(_1c(_1b[i],_1b[i-1])<0){
var q=_1b[i];
_1b[i]=_1b[i-1];
_1b[i-1]=q;
_1f=true;
}
}
b++;
}
}};
if(document.addEventListener){
document.addEventListener("DOMContentLoaded",sorttable.init,false);
}
if(/WebKit/i.test(navigator.userAgent)){
var _timer=setInterval(function(){
if(/loaded|complete/.test(document.readyState)){
sorttable.init();
}
},10);
}
window.onload=sorttable.init;
function dean_addEvent(_24,_25,_26){
if(_24.addEventListener){
_24.addEventListener(_25,_26,false);
}else{
if(!_26.$$guid){
_26.$$guid=dean_addEvent.guid++;
}
if(!_24.events){
_24.events={};
}
var _27=_24.events[_25];
if(!_27){
_27=_24.events[_25]={};
if(_24["on"+_25]){
_27[0]=_24["on"+_25];
}
}
_27[_26.$$guid]=_26;
_24["on"+_25]=handleEvent;
}
}
dean_addEvent.guid=1;
function removeEvent(_28,_29,_2a){
if(_28.removeEventListener){
_28.removeEventListener(_29,_2a,false);
}else{
if(_28.events&&_28.events[_29]){
delete _28.events[_29][_2a.$$guid];
}
}
}
function handleEvent(_2b){
var _2c=true;
_2b=_2b||fixEvent(((this.ownerDocument||this.document||this).parentWindow||window).event);
var _2d=this.events[_2b.type];
for(var i in _2d){
this.$$handleEvent=_2d[i];
if(this.$$handleEvent(_2b)===false){
_2c=false;
}
}
return _2c;
}
function fixEvent(_2f){
_2f.preventDefault=fixEvent.preventDefault;
_2f.stopPropagation=fixEvent.stopPropagation;
return _2f;
}
fixEvent.preventDefault=function(){
this.returnValue=false;
};
fixEvent.stopPropagation=function(){
this.cancelBubble=true;
};
if(!Array.forEach){
Array.forEach=function(_30,_31,_32){
for(var i=0;i<_30.length;i++){
_31.call(_32,_30[i],i,_30);
}
};
}
Function.prototype.forEach=function(_34,_35,_36){
for(var key in _34){
if(typeof this.prototype[key]=="undefined"){
_35.call(_36,_34[key],key,_34);
}
}
};
String.forEach=function(_38,_39,_3a){
Array.forEach(_38.split(""),function(chr,_3c){
_39.call(_3a,chr,_3c,_38);
});
};
var forEach=function(_3d,_3e,_3f){
if(_3d){
var _40=Object;
if(_3d instanceof Function){
_40=Function;
}else{
if(_3d.forEach instanceof Function){
_3d.forEach(_3e,_3f);
return;
}else{
if(typeof _3d=="string"){
_40=String;
}else{
if(typeof _3d.length=="number"){
_40=Array;
}
}
}
}
_40.forEach(_3d,_3e,_3f);
}
};

