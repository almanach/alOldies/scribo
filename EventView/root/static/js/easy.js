/*
  javascrip functions to help visualizing EASy annotation
*/

function relation_findPosX(obj) 
{
  var curleft = 0;
  if (obj.offsetParent) 
  {
    while (obj.offsetParent) 
        {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

function relation_findPosY(obj) 
{
    var curtop = 0;
    if (obj.offsetParent) 
    {
        while (obj.offsetParent) 
        {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
}

var saved = new Array;

function relation_hide(rid,elt) 
{
    rel = document.getElementById(rid); 
    rel.style.visibility = 'hidden';
    elt.style.backgroundColor = saved['relation'];
    var table = elt.getElementsByTagName('arg');
    for (i=0; i < table.length; i++ ) {
        var arg = document.getElementById(table[i].getAttribute('aid'));
        if (arg) {
            arg.style.backgroundColor=saved[table[i].getAttribute('aid')];
            arg.style.fontWeight = "lighter";
        }
    }
    saved = new Array;
}

function relation_show(rid,elt)
{
    rel = document.getElementById(rid);
    posX=290;
    posY=50;
    if ((rel.style.top == '' || rel.style.top == 0) 
        && (rel.style.left == '' || rel.style.left == 0))
    {
            // need to fixate default size (MSIE problem)
        rel.style.width = rel.offsetWidth + 'px';
        rel.style.height = rel.offsetHeight + 'px';

            // if tooltip is too wide, shift left to be within parent 
        if (posX + rel.offsetWidth > elt.offsetWidth) posX = elt.offsetWidth - rel.offsetWidth;
        if (posX < 0 ) posX = 0; 
        
        x = relation_findPosX(elt) + posX;
        y = relation_findPosY(elt) + posY;
        
        rel.style.top = y + 'px';
        rel.style.left = x + 'px';
        rel.style.opacity = .95;
        rel.style.backgroundColor = "#d6d6fc";
        
    }
    rel.style.visibility = 'visible';
    saved['relation'] = elt.style.backgroundColor;
    elt.style.backgroundColor = 'Gold';
    var table = elt.getElementsByTagName('arg');
    for (i=0; i < table.length; i++ ) {
        var arg = document.getElementById(table[i].getAttribute('aid'));
        if (arg) {
            saved[table[i].getAttribute('aid')] = arg.style.backgroundColor;
            arg.style.backgroundColor = 'Gold';
            arg.style.fontWeight = "bolder";
        }
    }
}

function URLencode(sStr) {
    return escape(sStr).
             replace(/\\+/g, '%2B').
                replace(/\\"/g,'%22'). //"
                   replace(/\\'/g, '%27'). //'
                     replace(/\//g,'%2F');
  }
