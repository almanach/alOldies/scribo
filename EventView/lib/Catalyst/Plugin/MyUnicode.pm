package Catalyst::Plugin::MyUnicode;

use strict;
use Encode;

our $VERSION = '0.8';

sub finalize {
    my $c = shift;
    
    if ( $c->response->{body} 
	 && utf8::is_utf8($c->response->{body}) 
	 && $c->response->content_encoding =~ /UTF-8/i
       ){
        utf8::encode( $c->response->{body} );
      }
    
    return $c->NEXT::finalize;
}

sub prepare_parameters {
    my $c = shift;

    $c->NEXT::prepare_parameters;

    for my $value ( values %{ $c->request->parameters } ) {
      
      next if ( ref $value && ref $value ne 'ARRAY' );

      for ( ref($value) ? @{$value} : $value ) {
	next if Encode::is_utf8($_);
	utf8::decode($_);
      }
      
    }
    
  }

sub prepare_query_parameters {
    my $c = shift;

    $c->NEXT::prepare_query_parameters;

    for my $value ( values %{ $c->request->query_parameters } ) {
      
      next if ( ref $value && ref $value ne 'ARRAY' );

      for ( ref($value) ? @{$value} : $value ) {
	next if Encode::is_utf8($_);
	utf8::decode($_);
      }
      
    }
    
  }

sub prepare_body_parameters {
    my $c = shift;

    # Read in the body params via the engine
    $c->NEXT::prepare_body_parameters;

    for my $value ( values %{ $c->request->body_parameters } ) {

      # This breaks decoding if a param is a hash
      # and Data::Visitor will return hash *keys* so that's no help
      next if ref $value && ref $value ne 'ARRAY';
      
      for ( ref( $value ) ? @{$value} : $value ) {
	
	# Don't decode if already decoded.
	next if Encode::is_utf8( $_ );

	$_ = decode_utf8( $_ );
      }
    }

    return;
  }

1;

__END__

=head1 NAME

Catalyst::Plugin::Unicode - Unicode aware Catalyst

=head1 SYNOPSIS

    use Catalyst qw[Unicode];


=head1 DESCRIPTION

On request, decodes all params from UTF-8 octets into a sequence of 
logical characters. On response, encodes body into UTF-8 octets.

=head1 OVERLOADED METHODS

=over 4

=item finalize

Encodes body into UTF-8 octets.

=item prepare_parameters

Decodes parameters into a sequence of logical characters.

=back

=head1 SEE ALSO

L<utf8>, L<Catalyst>.

=head1 AUTHORS

Christian Hansen, C<< <ch@ngmedia.com> >>

Marcus Ramberg, C<< <mramberg@pcan.org> >>

Jonathan Rockway C<< <jrockway@cpan.org> >>

=head1 LICENSE

This library is free software . You can redistribute it and/or modify 
it under the same terms as perl itself.

=cut
