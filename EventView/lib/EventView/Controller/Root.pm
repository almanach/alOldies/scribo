package EventView::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

EventView::Controller::Root - Root Controller for EventView

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub default : Private {
    my ( $self, $c ) = @_;
    $c->res->redirect( $c->uri_for('/view' ) );
}

sub auto : Private {
  my ($self, $c) = @_;
  
  # Allow unauthenticated users to reach the login page.  This
  # allows anauthenticated users to reach any action in the Login
  # controller.  To lock it down to a single action, we could use:
  #   if ($c->action eq $c->controller('Login')->action_for('index'))
  # to only allow unauthenticated access to the C<index> action we
  # added above.
  if ($c->controller eq $c->controller('Login')) {
    return 1;
  }

  # If a user doesn't exist, force login
  if (!$c->user_exists) {
    # Dump a log message to the development server debug output
    $c->log->debug('***Root::auto User not found, forwarding to /login');
    # Redirect the user to the login page
    $c->response->redirect($c->uri_for('/login'));
    # Return 0 to cancel 'post-auto' processing and prevent use of application
    return 0;
  }
  
  # User found, so return 1 to continue with processing after this 'auto'
  return 1;
}


sub end : ActionClass('RenderView') {}

=head1 AUTHOR

clergeri

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
