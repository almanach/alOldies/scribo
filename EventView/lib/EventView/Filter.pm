package EventView::Filter;
use base qw( Template::Plugin::Filter );
our $DYNAMIC = 1;
## use String::Approx 'amatch';
use Lingua::Stem::Fr qw/stem_word/;

sub filter {
  my ($self,$text,$arg,$conf) = @_;
  $conf = $self->merge_config($conf);
  my $ticket = $conf->{ticket};
  my $trole = $conf->{trole};
  my @words = (
	       [w_truncate(w_normalize($ticket->noun->label)) => 'noun'],
	       [w_truncate(w_normalize($ticket->verb->label)) => 'verb']
	      );
  foreach my $xl ($trole->lemma) {
    push(@words,[w_truncate($xl->lemma) => 'lemma']);
  }
  $text = join(' ',map {try_hilight($_,@words)} split(/\s+/,$text));
  return $text;
}

sub try_hilight {
  my ($token,@words) = @_;
  foreach my $xw (@words) {
    my ($w,$type) = @$xw;
    if ($token =~ /^\Q$w\E/i || $token =~ /'\Q$w\E/i) {
      return "<span class='hilight_$type'>$token</span>";
    }
  }
  return $token;
}

sub w_normalize {
  my $w = shift;
  $w =~ s/_[a-z]+$//o;
  return $w;
}

sub w_truncate {
  my $w = shift;
##  return length($w) > 5 ? substr($w,0,-2) : $w;
  return stem_word($w);
}

sub init {
  my $self = shift;
  my $name = $self->{ _CONFIG }->{ name } || 'hilight';
  $self->install_filter($name);
  return $self;
}


1;
