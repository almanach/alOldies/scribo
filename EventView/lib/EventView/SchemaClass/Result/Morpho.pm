package EventView::SchemaClass::Result::Morpho;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components(qw{InflateColumn::DateTime});

=head1 NAME

EventView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("morphology");

__PACKAGE__->add_columns(
  "tid",
  { data_type => "integer", is_nullable => 0 },
  "stem",
  { data_type => 'text' },
  "suffix",
  { data_type => 'text' },
);

__PACKAGE__->belongs_to(ticket => 'EventView::SchemaClass::Result::Ticket', 'tid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
