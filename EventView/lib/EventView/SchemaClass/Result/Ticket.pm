package EventView::SchemaClass::Result::Ticket;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

EventView::SchemaClass::Result::Term

=cut

__PACKAGE__->table("ticket");

__PACKAGE__->add_columns(
  "tid" => { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "vid" => { data_type => "integer", is_nullable => 0 },
  "nid" => { data_type => "integer", is_nullable => 0 },
  "shared" => { data_type => "integer", is_nullable => 1 },
  "confidence" => { data_type => "float", is_nullable => 1 },
  "rank" => { data_type => "integer", is_nullable => 1 },
  "status" => { data_type => "integer", is_nullable => 1 }
);

__PACKAGE__->set_primary_key("tid");

__PACKAGE__->has_many(morpho => 'EventView::SchemaClass::Result::Morpho', 'tid');
__PACKAGE__->has_many(roles => 'EventView::SchemaClass::Result::TRole', 'tid');

__PACKAGE__->belongs_to(verb => 'EventView::SchemaClass::Result::Word', 'vid');
__PACKAGE__->belongs_to(noun => 'EventView::SchemaClass::Result::Word', 'nid');

__PACKAGE__->has_many(related => 'EventView::SchemaClass::Result::Related', 'tid1');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
