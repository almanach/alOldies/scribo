package EventView::SchemaClass::Result::Sample;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

EventView::SchemaClass::Result::Sample

=cut

__PACKAGE__->table("sample");

=head1 ACCESSORS

=head2 sid

  data_type: 'integer'
  is_nullable: 1

=head2 fid

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "sid",
  { data_type => "integer", is_nullable => 0 },
  "fid",
  { data_type => "integer", is_nullable => 0 },
);

__PACKAGE__->belongs_to(sentence => 'EventView::SchemaClass::Result::Sentence', 'sid');
__PACKAGE__->belongs_to(trole => 'EventView::SchemaClass::Result::TRole', 'fid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
