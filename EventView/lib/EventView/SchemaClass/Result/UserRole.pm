package EventView::SchemaClass::Result::UserRole;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

=head1 NAME

EventView::SchemaClass::Result::UserRole

=cut

__PACKAGE__->table("user_roles");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  is_nullable: 0

=head2 role_id

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "role_id",
  { data_type => "integer", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("user_id", "role_id");

__PACKAGE__->belongs_to(user => 'EventView::SchemaClass::Result::User', 'user_id');
__PACKAGE__->belongs_to(role => 'EventView::SchemaClass::Result::Role', 'role_id');


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EpqcMg4sapTO8pQzOZkwtQ


# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
