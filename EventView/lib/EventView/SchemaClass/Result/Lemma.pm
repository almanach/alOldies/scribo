package EventView::SchemaClass::Result::Lemma;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

EventView::SchemaClass::Result::Lemma

=cut

__PACKAGE__->table("lemma");

__PACKAGE__->add_columns(
  "fid",
  { data_type => "integer", is_nullable => 0 },
  "lemma",
  { data_type => "text", is_nullable => 1 },
  "pos",
  { data_type => "text", is_nullable => 1 },
);

__PACKAGE__->belongs_to(trole => 'EventView::SchemaClass::Result::TRole', 'fid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
