package EventView::SchemaClass::Result::Related;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

EventView::SchemaClass::Result::Related

=cut

__PACKAGE__->table("related");

=head1 ACCESSORS

=head2 tid1

  data_type: 'integer'
  is_nullable: 1

=head2 tid2

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "tid1",
  { data_type => "integer", is_nullable => 0 },
  "tid2",
  { data_type => "integer", is_nullable => 0 },
);

__PACKAGE__->belongs_to(ticket1 => 'EventView::SchemaClass::Result::Ticket', 'tid1');
__PACKAGE__->belongs_to(ticket2 => 'EventView::SchemaClass::Result::Ticket', 'tid2');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
