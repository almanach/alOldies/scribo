package EventView::SchemaClass::Result::TRole;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components(qw{InflateColumn::DateTime});

=head1 NAME

EventView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("trole");

__PACKAGE__->add_columns(
 "fid",
  { data_type => "integer", is_nullable => 0 },
  "tid",
  { data_type => "integer", is_nullable => 0 },
  "vrole",
  { data_type => 'text' },
  "nrole",
  { data_type => 'text' },
  "w",
  { data_type => 'float' }			 
);

__PACKAGE__->set_primary_key("fid");


__PACKAGE__->belongs_to(ticket => 'EventView::SchemaClass::Result::Ticket', 'tid');
__PACKAGE__->has_many(lemma => 'EventView::SchemaClass::Result::Lemma', 'fid');
__PACKAGE__->has_many(samples => 'EventView::SchemaClass::Result::Sample', 'fid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
