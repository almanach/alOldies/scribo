package HypoView::Controller::Login;

use strict;
use warnings;
use base 'Catalyst::Controller';

sub index : Private {
  my ($self, $c) = @_;

  ##  $c->require_ssl;
  my $user = $c->request->param("username") || "";
  my $password = $c->request->param("password") || "";

  if ($user && $password)
    {
      if ( $c->authenticate( { username => $user, 
			       password => $password
			     } 
			   ) ) {
	$c->response->redirect($c->uri_for('/list'));
	return;
      } else {
	# login incorrect
	## $c->res->body( "bad $user");
	$c->stash->{error_msg} = "Bad username or password.";
      }
    }
  ##  $c->res->body( "waiting");
  $c->stash->{template} = 'login.tt2';
}


=head1 NAME

EasyRef::Controller::Login - Catalyst Controller

=head1 SYNOPSIS

See L<EasyRef>

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=over 4

#
# Uncomment and modify this or add new actions to fit your needs
#
#=item default
#
#=cut
#
#sub default : Private {
#    my ( $self, $c ) = @_;
#
#    # Hello World
#    $c->response->body('EasyRef::Controller::Login is on Catalyst!');
#}

=back


=head1 AUTHOR

Eric De la clergerie

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
