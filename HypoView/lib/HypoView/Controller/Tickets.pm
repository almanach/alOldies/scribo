package HypoView::Controller::Tickets;
use Moose;
use namespace::autoclean;
use Encode;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

HypoView::Controller::Tickets - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched HypoView::Controller::Tickets in Tickets.');
}

sub view :Global {
   my ( $self, $c ) = @_;
   $c->stash->{template} = 'entries.tt2';
}


sub make_widget {
  my ($self,$c) = @_;

  my $w = $c->widget('tickets')->method('get');

  # Create the form fields
  $w->element('Textfield', 'rank')->label('Enter rank (or start:end:label)')->size(20);
  $w->filter( TrimEdges => 'rank' );
  return $w;
}

sub list : Global {
  my ($self,$c) = @_;
  my $rank = $c->req->parameters->{'rank'};

  unless (Encode::is_utf8( $rank ) ) {
    $rank = decode_utf8( $rank );
  }

  my $w=$self->make_widget($c);
  $c->action($c->uri_for('/list'));

  my $result = $w->process($c->req);
  $c->stash->{widget_result} = $result;

  my $query = {};
  my $options = {};

  ##  my $rank = $c->req->param('rank');
  my $xrank=$rank;

  $xrank ||= 1;
  $xrank =~ s/^\s+//og;
  if ($xrank !~ /^\d+$/ && $xrank !~ /:/) {
    $xrank = "::$xrank";
  }
  if ($xrank =~ /:/) {
    $self->find_rank($c,$xrank);
  } else {
    my $delta = (($xrank > 40) ? 20 : (($xrank > 20) ? $xrank/2 : 0)) * 23.3;
    $c->stash->{delta} = $delta;
    $self->get_entries($c,$xrank);
  }
  $c->stash->{template} = "entries.tt2";
  my $current = $c->stash->{ticket};
  my $sentences = $c->stash->{sentences} = [];
  return unless ($current);
  my $current_tid = $current->tid;
}

sub get_entries {
  my ($self,$c,$rank) = @_;
  my $min = $rank - 20;
  $min = 1 if ($min < 1);
  my $max = $min + 50;
  my $info = $c->stash->{entries} = [];
  my $k = $min-1;
  my $ers = $c->model('HypoView::Ticket')
    ->search( {
	       tid => [ -and => { '>=' , $min }, {'<=' , $max } ]
	      },
	      { 
	       order_by => 'tid'
	      }
	    );
  while (my $entry = $ers->next) {
    push(@$info,$entry);
    ($entry->tid == $rank) and $c->stash->{ticket} = $entry;
  }
}

sub find_rank {
  my ($self,$c,$rank) = @_;
  my ($start,$end,$form) = split(/:/,$rank);
  my @rs = $c->model('HypoView::Ticket')
    ->search({ label => {-like => "\%$form\%"}});
  $c->stash->{entries} = [@rs];
  $c->stash->{ticket} = $rs[0];
}

sub changeStatus : Local {
  my ($self,$c,$tid,$v) = @_;
  if (my $term = $c->model('HypoView::Ticket')->find($tid)) {
    $term->status($v);
    $term->update;
    $c->stash->{entry} = $c->model('HypoView::Ticket')->find($tid);
    $c->stash->{template} = 'candidate.tt2';
    $c->forward('HypoView::View::Simple');
  } else {
    my $s = "Missing ticket";
    $c->res->output($s);
    $c->res->status(404);
  }
}

=head1 AUTHOR

clergeri

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
