package HypoView::SchemaClass::Result::Instance;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components(qw{InflateColumn::DateTime});

=head1 NAME

HypoView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("instance");

__PACKAGE__->add_columns(
  "wid",
  { data_type => "integer", is_nullable => 0 },
  "tid",
  { data_type => 'integer' , is_nullable => 0 }
);

__PACKAGE__->belongs_to(ticket => 'HypoView::SchemaClass::Result::Ticket', 'tid');
__PACKAGE__->belongs_to(word => 'HypoView::SchemaClass::Result::Word', 'wid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
