package HypoView::SchemaClass::Result::Word;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components(qw{InflateColumn::DateTime});

=head1 NAME

HypoView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("word");

__PACKAGE__->add_columns(
  "label",
  { data_type => "text", is_nullable => 0 },
  "wid",
  { data_type => 'integer' , is_nullable => 0, is_auto_increment => 1 }
);

__PACKAGE__->set_primary_key("wid");

__PACKAGE__->has_many(instances => 'HypoView::SchemaClass::Result::Instance', 'wid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
