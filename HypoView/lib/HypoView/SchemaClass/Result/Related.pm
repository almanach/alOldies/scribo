package HypoView::SchemaClass::Result::Related;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

HypoView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("related");

__PACKAGE__->add_columns(
  "src",
  { data_type => 'integer' , is_nullable => 0 },
  "target",
  { data_type => 'integer' , is_nullable => 0 }
);

__PACKAGE__->belongs_to(xsrc => 'HypoView::SchemaClass::Result::Ticket', 'src');
__PACKAGE__->belongs_to(xtarget => 'HypoView::SchemaClass::Result::Ticket', 'target');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
