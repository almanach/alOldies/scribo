package HypoView::SchemaClass::Result::Ticket;

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

HypoView::SchemaClass::Result::Term

=cut

__PACKAGE__->table("ticket");

__PACKAGE__->add_columns(
  "tid" => { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label" => { data_type => "text", is_nullable => 0 },
  "type" => { data_type => "text", is_nullable => 0 },
  "status" => { data_type => "integer", is_nullable => 1 }
);

__PACKAGE__->set_primary_key("tid");

__PACKAGE__->has_many(sentences => 'HypoView::SchemaClass::Result::Sentence', 'tid');

__PACKAGE__->has_many(instances => 'HypoView::SchemaClass::Result::Instance', 'tid');

__PACKAGE__->has_many(related => 'HypoView::SchemaClass::Result::Related', 'src');
__PACKAGE__->has_many(related2 => 'HypoView::SchemaClass::Result::Related', 'target');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
