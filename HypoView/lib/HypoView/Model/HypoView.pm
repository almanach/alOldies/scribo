package HypoView::Model::HypoView;

use strict;
use base 'Catalyst::Model::DBIC::Schema';

__PACKAGE__->config(
    schema_class => 'HypoView::SchemaClass',
);

=head1 NAME

HypoView::Model::HypoView - Catalyst DBIC Schema Model

=head1 SYNOPSIS

See L<HypoView>

=head1 DESCRIPTION

L<Catalyst::Model::DBIC::Schema> Model using schema L<HypoView::SchemaClass>

=head1 GENERATED BY

Catalyst::Helper::Model::DBIC::Schema - 0.43

=head1 AUTHOR

clergeri

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
