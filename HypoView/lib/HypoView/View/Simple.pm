package HypoView::View::Simple;

use strict;
use Class::C3;
use base 'Catalyst::View::TT';
##use NEXT;

__PACKAGE__->config(
		    TEMPLATE_EXTENSION => '.tt2',
		    CATALYST_VAR => 'Catalyst',
		    INCLUDE_PATH => [
				     HypoView->path_to( 'root', 'src' ),
#				     HypoView->path_to( 'root', 'lib' )
				    ],
		    ERROR        => 'error.tt2',
		    TIMER        => 0,
		    COMPILE_DIR => '/tmp/template_cache'
		   );

sub process {
  my ( $self, $c ) = @_;
  $c->response->content_type('text/html')
    unless ($c->res->content_type);
  $c->response->content_encoding('utf-8')
    unless($c->res->content_encoding);
##  $self->NEXT::process($c);
  $self->next::method($c);
  $c->log->info('Simple '.$c->res->content_type);
  $c->log->info('Simple '.$c->res->content_encoding);
  $c->response->body(utf8::encode($c->response->{body}))
    unless ($c->res->content_type =~ /xml/);
}

=head1 NAME

HypoView::View::Simple - TT View for HypoView

=head1 DESCRIPTION

TT View for HypoView. 

=head1 AUTHOR

=head1 SEE ALSO

L<HypoView>

De la Clergerie Eric

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
