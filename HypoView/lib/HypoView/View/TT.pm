package HypoView::View::TT;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config({
    CATALYST_VAR => 'Catalyst',
    INCLUDE_PATH => [
        HypoView->path_to( 'root', 'src' ),
        HypoView->path_to( 'root', 'lib' )
    ],
    PRE_PROCESS  => 'config/main',
    WRAPPER      => 'site/wrapper',
    ERROR        => 'error.tt2',
    TIMER        => 0,
    COMPILE_DIR => '/tmp/template_cache',
    TEMPLATE_EXTENSION => '.tt2',
    render_die => 1,
});

sub process {
  my ( $self, $c ) = @_;
  $c->response->content_type('text/html')
    unless ($c->res->content_type);
  $c->response->content_encoding('utf-8')
    unless($c->res->content_encoding);
##  $self->NEXT::process($c);
  $self->next::method($c);
  $c->log->info('TT '.$c->res->content_type);
  $c->log->info('TT '.$c->res->content_encoding);
  $c->response->body(utf8::encode($c->response->{body}))
    unless ($c->res->content_type =~ /xml/);
}

=head1 NAME

HypoView::View::TT - TT View for HypoView

=head1 DESCRIPTION

TT View for HypoView.

=head1 SEE ALSO

L<HypoView>

=head1 AUTHOR

clergeri

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
