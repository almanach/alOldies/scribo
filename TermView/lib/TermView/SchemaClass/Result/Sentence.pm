package TermView::SchemaClass::Result::Sentence;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

TermView::SchemaClass::Result::Sentence

=cut

__PACKAGE__->table("sentence");

=head1 ACCESSORS

=head2 sid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 text

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "sid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "text",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("sid");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Dix24f24Z7L+796+PMeL2g

__PACKAGE__->belongs_to(sentence => 'TermView::SchemaClass::Result::Term', 'sid');
__PACKAGE__->has_many(variant => 'TermView::SchemaClass::Result::Variant', 'vid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
