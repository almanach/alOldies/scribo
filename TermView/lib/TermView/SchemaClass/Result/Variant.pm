package TermView::SchemaClass::Result::Variant;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

TermView::SchemaClass::Result::Variant

=cut

__PACKAGE__->table("variant");

=head1 ACCESSORS

=head2 vid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 term

  data_type: 'text'
  is_nullable: 1

=head2 tid

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "vid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "term",
  { data_type => "text", is_nullable => 1 },
  "tid",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("vid");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:30KO7oi02a4RKdIbuvFPbw

__PACKAGE__->add_columns(
			 "freq",
			 { data_type => "integer", is_nullable => 1 },
			 "status",
			 { data_type => "integer", is_nullable => 1 }
			);

__PACKAGE__->belongs_to(model => 'TermView::SchemaClass::Result::Term', 'tid');
__PACKAGE__->has_many(sample => 'TermView::SchemaClass::Result::Sample', 'vid');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
