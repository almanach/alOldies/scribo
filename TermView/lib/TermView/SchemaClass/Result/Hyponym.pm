package TermView::SchemaClass::Result::Hyponym;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

TermView::SchemaClass::Result::Hyponym

=cut

__PACKAGE__->table("hyponym");

__PACKAGE__->add_columns(
  "hyper",
  { data_type => "integer", is_nullable => 0 },
  "hypo",
  { data_type => "integer", is_nullable => 0 },
);

# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:30KO7oi02a4RKdIbuvFPbw

__PACKAGE__->belongs_to(xhyper => 'TermView::SchemaClass::Result::Term', 'hyper');
__PACKAGE__->belongs_to(xhypo => 'TermView::SchemaClass::Result::Term', 'hypo');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
