package TermView::SchemaClass::Result::Sample;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

TermView::SchemaClass::Result::Sample

=cut

__PACKAGE__->table("sample");

=head1 ACCESSORS

=head2 sid

  data_type: 'integer'
  is_nullable: 1

=head2 vid

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "sid",
  { data_type => "integer", is_nullable => 1 },
  "vid",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4ycHUyTxjcLN8tyW9qhZiw

__PACKAGE__->belongs_to(sentence => 'TermView::SchemaClass::Result::Sentence', 'sid');


# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
