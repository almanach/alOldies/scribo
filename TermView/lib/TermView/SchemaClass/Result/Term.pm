package TermView::SchemaClass::Result::Term;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 NAME

TermView::SchemaClass::Result::Term

=cut

__PACKAGE__->table("term");

=head1 ACCESSORS

=head2 tid

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 model

  data_type: 'text'
  is_nullable: 1

=head2 w1

  data_type: 'float'
  is_nullable: 1

=head2 w2

  data_type: 'float'
  is_nullable: 1

=head2 freq

  data_type: 'integer'
  is_nullable: 1

=head2 standalone1

  data_type: 'integer'
  is_nullable: 1

=head2 standalone2

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "tid",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "model",
  { data_type => "text", is_nullable => 1 },
  "w1",
  { data_type => "float", is_nullable => 1 },
  "w2",
  { data_type => "float", is_nullable => 1 },
  "freq",
  { data_type => "integer", is_nullable => 1 },
  "standalone1",
  { data_type => "integer", is_nullable => 1 },
  "standalone2",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("tid");


# Created by DBIx::Class::Schema::Loader v0.07002 @ 2010-10-13 22:06:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TwrIhH52VjQQcRki0o9d5g


__PACKAGE__->add_columns(
			 "status",
			 { data_type => "integer", is_nullable => 1 }
			);

__PACKAGE__->has_many(variant => 'TermView::SchemaClass::Result::Variant', 'tid');

__PACKAGE__->has_many(hyponyms => 'TermView::SchemaClass::Result::Hyponym', 'hyper');

__PACKAGE__->has_many(hyperonyms => 'TermView::SchemaClass::Result::Hyponym', 'hypo');

# You can replace this text with custom content, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
