package TermView::Controller::Terms;
use Moose;
use namespace::autoclean;
use Encode;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

TermView::Controller::Terms - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched TermView::Controller::Terms in Terms.');
}

sub view :Global {
   my ( $self, $c ) = @_;
   $c->stash->{template} = 'entries.tt2';
}


sub make_widget {
  my ($self,$c) = @_;

  my $w = $c->widget('terms')->method('get');

  # Create the form fields
  $w->element('Textfield', 'rank')->label('Enter rank (or start:end:term)')->size(20);
  $w->filter( TrimEdges => 'rank' );
  return $w;
}

sub list : Global {
  my ($self,$c) = @_;
  my $rank = $c->req->parameters->{'rank'};

  unless (Encode::is_utf8( $rank ) ) {
    $rank = decode_utf8( $rank );
  }

  my $w=$self->make_widget($c);
  $c->action($c->uri_for('/list'));

  my $result = $w->process($c->req);
  $c->stash->{widget_result} = $result;

  my $query = {};
  my $options = {};

  ##  my $rank = $c->req->param('rank');
  my $xrank=$rank;

  $xrank ||= 1;
  $xrank =~ s/^\s+//og;
  if ($xrank !~ /^\d+$/ && $xrank !~ /:/) {
    $xrank = "::$xrank";
  }
  if ($xrank =~ /:/) {
    $self->find_rank($c,$xrank);
  } else {
    my $delta = (($xrank > 40) ? 20 : (($xrank > 20) ? $xrank/2 : 0)) * 23.3;
    $c->stash->{delta} = $delta;
    $self->get_entries($c,$xrank);
  }
  $c->stash->{template} = "entries.tt2";
  my $current = $c->stash->{term};
  my $sentences = $c->stash->{sentences} = [];
  return unless ($current);
  my $variants = $current->variant;
  my $model = $current->model;
  my @model = ();
  my $current_tid = $current->tid;
  foreach my $m (split(/__/,$model)) {
    my ($type,$content) = $m =~ /\{:(\S+)\s+(.+?)\s+\S+:\}/;
    push(@model,{type => $type, content => $content });
  }
  $c->stash->{chunks} = [@model];
  my $governor;
  while ($model[0]{content} =~ m{(.+?)\/(nc|np|adj|adv|v)}og) {
    my $type = $2;
    my $comp = $1;
    my $xcomp = lc($comp);
    $xcomp =~ s/:_Uv$//o;
    if ($type eq 'np' || $type eq 'nc') {
      my $x = $c->stash->{hyperonym} = "${comp}/${type}";
      my @related =
	$c->model('TermView::Term')
	  ->search({model => { -like => "\%$x\%" }});
      $c->stash->{related} = [grep {$_->tid != $current_tid
				      && has_governor($_->model,$xcomp)
				    } @related];
      @{$c->stash->{related}} or delete $c->stash->{related};
      last;
    }
    if (my $hyponyms = $current->hyponyms) {
      $c->stash->{hyponyms} = $hyponyms;
    }
  }
  while (my $variant = $variants->next) {
    my $v = $variant->term;
    my $qv = $v;
    $qv =~ s/\s+/\\s+/og;
    my $samples = $variant->sample;
    while (my $s = $samples->next) {
      my $label = $s->sentence->label;
      my $text = $s->sentence->text;
      $text =~ s/\\([(){}%\\])/$1/og;
      $text =~ s/\s+([,;\.%])/$1/og;
      $text =~ s/(\d+,)\s+(\d+)/$1$2/og;
      $text =~ s{($qv)}{<span class="occ">$1</span>}gi;
      push(@$sentences,{ label => $label, text => $text });
    }
  }
}

sub has_governor {
  my $model = shift;
  my $lemma = shift;
  my $chunk = (split(/__/,$model))[0];
  my ($ws) = $chunk =~ m/\{:\S+\s+(.*?)\s*\S+\}/;
  while ($ws =~ m{\G\s*(.+?)/(?:np|adv|adj|det|prep|nc|number|predet|advPref|title|_|coo|v|ncpred|adjPref)}og) {
    my $u = lc($1);
    $u =~ s/:_Uv$//o;
    ($lemma eq $u) and return 1;
  }
  return 0;
}

sub get_entries {
  my ($self,$c,$rank) = @_;
  my $min = $rank - 20;
  $min = 1 if ($min < 1);
  my $max = $min + 50;
  my $info = $c->stash->{entries} = [];
  my $k = $min-1;
  my $ers = $c->model('TermView::Term')
    ->search( {
	       tid => [ -and => { '>=' , $min }, {'<=' , $max } ]
	      },
	      { 
	       order_by => 'tid'
	      }
	    );
  while (my $entry = $ers->next) {
    push(@$info,$entry);
    ($entry->tid == $rank) and $c->stash->{term} = $entry;
  }
}

sub find_rank {
  my ($self,$c,$rank) = @_;
  my ($start,$end,$form) = split(/:/,$rank);
  my @rs = $c->model('TermView::Term')
    ->search({ model => {-like => "\%$form\%"}});
  $c->stash->{entries} = [@rs];
  $c->stash->{term} = $rs[0];
}

sub changeStatus : Local {
  my ($self,$c,$tid,$v) = @_;
  if (my $term = $c->model('TermView::Term')->find($tid)) {
    $term->status($v);
    $term->update;
    $c->stash->{entry} = $c->model('TermView::Term')->find($tid);
    $c->stash->{template} = 'candidate.tt2';
    $c->forward('TermView::View::Simple');
  } else {
    my $s = "Missing term";
    $c->res->output($s);
    $c->res->status(404);
  }
}

=head1 AUTHOR

clergeri

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
