use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'TermView' }
BEGIN { use_ok 'TermView::Controller::Terms' }

ok( request('/terms')->is_success, 'Request should succeed' );
done_testing();
